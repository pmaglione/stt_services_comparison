# Test different STT services with/without streaming


## 1) Install required libs
- Install required pip librarys: pip install -r requirements.txt
- Install SpeechRecognition lib last version
	- Go to speech_recognition folder
	- Exec: python setup.py

## 2) Exec tests

All the audios in audios folder will be transcribed, for no-stream option, audios should be 16khz

- python compare_stt_services_no_stream.py

or

- python compare_stt_services_stream.py